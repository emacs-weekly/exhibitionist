;; Setup package management
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Install and configure dependencies
(use-package htmlize
  :ensure t
  :config
  (setq org-html-htmlize-output-type 'css)
  (setq org-html-head-include-default-style nil))
