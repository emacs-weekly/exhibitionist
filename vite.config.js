import { configDefaults, defineConfig } from 'vitest/config'

export default defineConfig({
  base: '',
  test: {
    deps: {
      inline: [/vite-test-utils/]
    }
  }
})
