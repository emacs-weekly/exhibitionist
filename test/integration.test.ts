import { setup, createPage } from 'vite-test-utils'
import { it, describe, expect, beforeEach } from 'vitest';

await setup({
  browser: true
})

it('render the first slide', async () => {
  const page = await createPage('/')
  const html = await page.innerHTML('body')

  expect(html).toContain('TDD')
})

it('render next slide by press arrow right key', async () => {
  const page = await createPage('/')
  await page.keyboard.down('ArrowRight');
  expect(await page.innerHTML('body')).toContain('PDF')
})

it('render next slide by click arrow right', async () => {
  const page = await createPage('/')
  await page.locator("#right").click();
  expect(await page.innerHTML('body')).toContain('PDF')
})

it('render following slide by press space key', async () => {
  const page = await createPage('/')
  await page.keyboard.down(' ');
  expect(await page.innerHTML('body')).toContain('PDF')
})

