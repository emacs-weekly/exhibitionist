import fs from 'fs';
import path from 'path';

import { it, describe, expect } from 'vitest';
import { Parser } from '../src/model';

/**
* @vitest-environment jsdom
*/
describe('Parse html', () => {
    it("should parse slides top nodes", () => {
        const html = fs.readFileSync(path.resolve(__dirname, './test.html'), 'utf8');
        document.documentElement.innerHTML = html.toString();
        let parser = new Parser();
        let presentation = parser.parse(document);

        expect(presentation.slides.length).toBe(4);
        expect(presentation.slides[1].id).toEqual("orgd023ea9");
    });

    it("should parse slides with nested nodes", () => {
        const html = fs.readFileSync(path.resolve(__dirname, './test.html'), 'utf8');
        document.documentElement.innerHTML = html.toString();
        let parser = new Parser();
        let presentation = parser.parse(document);

        expect(presentation.slides[3].children.length).toBe(2);
    });


    it("should parse presentation author name", () => {
        const html = fs.readFileSync(path.resolve(__dirname, './test.html'), 'utf8');
        document.documentElement.innerHTML = html.toString();
        let parser = new Parser()
        let presentation = parser.parse(document);

        expect(presentation.slides[0].content.includes("John")).toBeTruthy();
    });

});
