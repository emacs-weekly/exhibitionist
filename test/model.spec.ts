import { it, describe, expect, beforeEach } from 'vitest';
import { Presentation, Slide } from '../src/model';

describe('Presentation model with one level', () => {
    let presentation: Presentation;
    let slide1: Slide;
    let slide2: Slide;

    beforeEach(() => {
        presentation = new Presentation();
        slide1 = new Slide();
        slide1.content = "one";
        slide2 = new Slide();
        slide2.content = "two";
        presentation.slides = [slide1, slide2];
    });

    // 一个 presentation 可以包含多个 slide
    it("should presentation has multi slides ", () => {
        expect(presentation.slides.length).toBe(2);
    });

    // presentation 默认展示第一个 slide
    it("should presentation show current slide", () => {
        expect(presentation.currentSlide()).toEqual(slide1);
    });

    // presentation 的 next 可以展示下一个 slide
    it("should presentation show next slide", () => {
        presentation.next();
        expect(presentation.currentSlide()).toEqual(slide2);
    });

    // presentation 的 prev 可以展示上一个 slide
    it("should presentation show prev slide", () => {
        presentation.slideIndex = 1;
        presentation.prev();
        expect(presentation.currentSlide()).toEqual(slide1);
    });

    // presentation 的 hasNext 在第一个 slide 时应该是 true
    it("should presentation has next slide", () => {
        expect(presentation.hasNext()).toBeTruthy();
    });

    // presentation 的 hasNext 在最后一个 slide 时应该是 false
    it("should presentation has no next slide", () => {
        presentation.slideIndex = 1;
        expect(presentation.hasNext()).toBeFalsy();
    });

    // presentation 的 hasPrev 在第一个 slide 时应该是 false
    it("should presentation has no prev slide", () => {
        expect(presentation.hasPrev()).toBeFalsy();
    });

    // presentation 的 hasPrev 在最后 slide 时应该是 true
    it("should presentation has prev slide", () => {
        presentation.slideIndex = 1;
        expect(presentation.hasPrev()).toBeTruthy();
    });

    // presentation 在 第一个 silde 时调用 prev 应该还是显示第一个 slide
    it("should presentation still show first slide if invoke prev when current slide is the first one", () => {
        presentation.prev();
        expect(presentation.currentSlide()).toEqual(slide1);
    });

    // presentation 在 最后一个 silde 时调用 next 应该还是显示最后一个 slide
    it("should presentation still show last slide if invoke next when current slide is the last one", () => {
        presentation.slideIndex = 1;
        presentation.next();
        expect(presentation.currentSlide()).toEqual(slide2);
    });
});

describe('Presentation with hash', () => {
    let presentation: Presentation;
    let slide1: Slide;
    let slide2: Slide;
    let slide2_1: Slide;
    let slide2_2: Slide;

    beforeEach(() => {
        presentation = new Presentation();
        slide1 = new Slide();
        slide1.id = "11";
        slide1.content = "one";
        slide2 = new Slide();
        slide2.id = "22";
        slide2.content = "two";
        slide2_1 = new Slide();
        slide2_1.id = "221";
        slide2_1.content = "2-1";
        slide2_1.parent = slide2;
        slide2_2 = new Slide();
        slide2_2.id = "222";
        slide2_2.parent = slide2;
        slide2_2.content = "2-2";
        slide2.children = [slide2_1, slide2_2];
        presentation.slides = [slide1, slide2];
    });

    it("should presentation show current slide id", () => {
        const origin_hash = presentation.currentSlide().id;
        presentation.next();
        const current_hash = presentation.currentSlide().id;
        expect(origin_hash).not.to.equal(current_hash);
    });

    it("should presentation set current slide by hash", () => {
        presentation.setCurrentSlide("22");
        expect(slide2).toEqual(presentation.currentSlide());
    });

    it("should presentation set current child slide by hash", () => {
        presentation.setCurrentSlide("222");
        expect(slide2_2).toEqual(presentation.currentSlide());
    });

});

describe('Presentation model with nested slide', () => {
    let presentation: Presentation;
    let slide1: Slide;
    let slide2: Slide;
    let slide2_1: Slide;
    let slide2_2: Slide;

    beforeEach(() => {
        presentation = new Presentation();
        slide1 = new Slide();
        slide1.content = "one";
        slide2 = new Slide();
        slide2.content = "two";
        slide2_1 = new Slide();
        slide2_1.content = "2-1";
        slide2_1.parent = slide2;
        slide2_2 = new Slide();
        slide2_2.parent = slide2;
        slide2_2.content = "2-2";
        slide2.children = [slide2_1, slide2_2];
        presentation.slides = [slide1, slide2];
    });

    // presentation 的 hasUp 和 hasDown 和当前 Slide 是否有 children 一致
    it("should presentation has up and down to be false if current slide has no child", () => {
        expect(presentation.hasUp()).toBeFalsy();
        expect(presentation.hasDown()).toBeFalsy();
        presentation.next();
        expect(presentation.hasUp()).toBeFalsy();
        expect(presentation.hasDown()).toBeTruthy();
    });


    // presentation 的 up 和 down 在当前 slide 没有 child 的时候不会改变当前 slide
    it("should presentation up(down) not change current slide if current slide has no child", () => {
        let currentSlide = presentation.currentSlide;
        presentation.up();
        let after_up_slide = presentation.currentSlide;
        presentation.down();
        let after_down_slide = presentation.currentSlide;
        expect(currentSlide).toEqual(after_up_slide);
        expect(currentSlide).toEqual(after_down_slide);
    });

    // presentation 的 up 和 down 在当前 slide 有 child 的时候会改变当前 slide 为其对应的 child
    it("should presentation up (down) will change to current slide children", () => {
        presentation.next(); // move to slide2
        presentation.down();
        expect(presentation.currentSlide()).toEqual(slide2_1);
        presentation.down();
        expect(presentation.currentSlide()).toEqual(slide2_2);
    });

    //  presentation 的 up 和 down 在没有 children 的情况下不会改变当前 slide 的状态
    it("should presentation up (down) not change current slide if has not child", () => {
        presentation.up();
        expect(presentation.currentSlide()).toEqual(slide1);
        presentation.down();
        expect(presentation.currentSlide()).toEqual(slide1);
    });

    // presentation 的 down 在有 children 时碰到边界时不会改变当前 slide 的状态
    it("should presentation up (down) not change current slide if it is the parent (children is the last one)", () => {
        presentation.next();
        presentation.up();
        expect(presentation.currentSlide()).toEqual(slide2);

        presentation.down();
        presentation.down();
        presentation.down();
        expect(presentation.currentSlide()).toEqual(slide2_2);
    });

    // presentation 的 prev 和 next 在当前 slide 是 child 时，应该直接改变为前一个（后一个）父级 slide
    it("should presentation prev (next) change to the father slide", () => {
        presentation.next(); // move to slide2
        presentation.down();
        presentation.prev();
        expect(presentation.currentSlide()).toEqual(slide1);
    });

    // presentation 的 prev 和 next 应该保留当前 slide 的当前 child 状态
    it("should presentation prev (next) restore the child slide", () => {
        presentation.next(); // move to slide2
        presentation.down();
        presentation.prev();
        presentation.next(); // move to slide2
        expect(presentation.currentSlide()).toEqual(slide2_1);
    });

    // presentation 的 第一个 child 再 up 应该返回父级 slide
    it("should presentation back to parent slide if invoke up when current slide is child", () => {
        presentation.next(); // move to slide2
        presentation.down();
        presentation.up();
        expect(presentation.currentSlide()).toEqual(slide2);
    });

    it("should presenation down || next", () => {
        presentation.following(); // 没有 down 所以直接 next
        expect(presentation.currentSlide()).toEqual(slide2);
        
        presentation.following(); // 有 down 所以 down
        expect(presentation.currentSlide()).toEqual(slide2_1);

        presentation.following(); // 有 down 所以 down
        expect(presentation.currentSlide()).toEqual(slide2_2);

        presentation.following(); // 最后一页还是最后一页
        expect(presentation.currentSlide()).toEqual(slide2_2);
    });
});
