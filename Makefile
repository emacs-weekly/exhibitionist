generate_demo_html_from_readme:
	@emacs --batch --no-init-file -l export.el README.org --funcall org-html-export-to-html --kill	
	@awk '/<\/head>/ {print "<link rel=\"icon\" href=\"\/favicon.png\" type=\"image\/png\" \/><script type=\"module\" src=\"\/src\/main.ts\"><\/script>"}1' README.html > index.html
	rm README.html

format_css:
	npx prettier --write src/style.scss 