export class Slide {
    currentIndex: number = -1;
    id: string = "";
    content: string = "";
    children: Slide[] = [];
    parent: Slide | undefined = undefined;

    hasDown(): boolean {
        if ((this.children.length) > 0 && (this.children.length > this.currentIndex + 1)) {
            return true;
        }
        return false;
    }

    hasUp(): boolean {
        if (this.currentIndex > -1) {
            return true;
        }
        return false;
    }

    next(): void {
        if (this.hasDown()) {
            this.currentIndex++;
        }
    }

    prev(): void {
        if (this.hasUp()) {
            this.currentIndex--;
        }
    }

    currentSlide(): Slide {
        if (this.currentIndex == -1) {
            return this;
        } else {
            return this.children[this.currentIndex];
        }
    }
}

export class Presentation {
    slides: Slide[] = [];
    slideIndex: number = 0;

    hasNext(): boolean {
        return (this.slides.length > this.slideIndex + 1);
    }

    hasPrev(): boolean {
        return (this.slideIndex > 0);
    }

    hasUp(): boolean {
        return this.slides[this.slideIndex].hasUp();
    }

    hasDown(): boolean {
        return this.slides[this.slideIndex].hasDown();
    }

    next(): void {
        if (this.hasNext()) {
            this.slideIndex++;
        }
    }

    prev(): void {
        if (this.hasPrev()) {
            this.slideIndex--;
        }
    }

    down(): void {
        if (this.hasDown()) {
            this.slides[this.slideIndex].next();
        }
    }

    up(): void {
        if (this.hasUp()) {
            this.slides[this.slideIndex].prev();
        }
    }

    following(): void {
        if (this.hasDown()) {
            this.down()
        } else {            
            this.next();
        }
    }

    setCurrentSlide(hash: string) {
        let result: Slide = this.slides[0];

        this.slides.forEach(s => {
            if (s.id == hash) {
                result = s;
                return;
            }
        });

        this.slides.forEach(s => {
            s.children.forEach(s => {
                if (s.id == hash) {
                    result = s;
                    return;
                }
            });
        });


        if (result.parent == undefined) {
            this.slideIndex = this.slides.indexOf(result);
        } else {
            this.slideIndex = this.slides.indexOf(result.parent);
            result.parent.currentIndex = result.parent.children.indexOf(result);
        }
    }

    currentSlide(): Slide {
        let result = this.slides[this.slideIndex].currentSlide();
        return result;
    }
}

export class Parser {
    parse(document: Document): Presentation {
        const outline2 = document.getElementsByClassName("outline-2") as HTMLCollectionOf<HTMLElement>;
        const outline2List = Array.from(outline2);
        let presentation = new Presentation();

        if (document.getElementsByTagName('h1').length > 0) {
            let slideH1 = new Slide();
            slideH1.content = document.getElementsByTagName('h1')[0].outerHTML;
            let metaAuthor = document.querySelector('meta[name="author"]');
            if (metaAuthor) {
                slideH1.content += ("<p class='author'>" + metaAuthor.getAttribute('content') + "</p>");
            }
            presentation.slides.push(slideH1);
        }

        outline2List.forEach(o2 => {
            let slide = new Slide();

            const outline3 = o2.getElementsByClassName("outline-3");
            if (outline3.length > 0) {
                const outline3List = Array.from(outline3);
                outline3List.forEach(o3 => {
                    let o3Slide = new Slide();
                    o3Slide.id = o3.getElementsByTagName('h3')[0].getAttribute('id')!;
                    o3Slide.content = o3.innerHTML;
                    slide.children.push(o3Slide);
                });
                slide.content =
                    o2.getElementsByTagName('h2')[0].outerHTML + o2.getElementsByClassName('outline-text-2')[0].innerHTML;
            } else {
                slide.content = o2.innerHTML;
            }
            slide.id = o2.getElementsByTagName('h2')[0].getAttribute('id')!;
            presentation.slides.push(slide);
            slide.children.forEach(o3 => o3.parent = slide);
        });
        return presentation;
    }
}
