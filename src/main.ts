import './style.scss'
import { Parser } from './model';

let parser = new Parser();
let presentation = parser.parse(document);

let currentHash = window.location.hash.substring(1);
presentation.setCurrentSlide(currentHash);

document.querySelector<HTMLDivElement>('body')!.innerHTML = '';
let app = document.createElement('div');
app.setAttribute("id", "app");
let nav = document.createElement('div');
nav.setAttribute("id", "nav");
document.body.append(app);
document.body.append(nav);
initNav();
render();

document.addEventListener('keydown', (e: KeyboardEvent) => {    
    switch (e.key) {  
        case ' ': {
            presentation.following();
            render();
            break;
        }      
        case 'ArrowUp': {
            presentation.up();
            render();
            break;
        }
        case 'ArrowLeft': {
            presentation.prev();
            render();
            break;
        }
        case 'ArrowRight': {
            presentation.next();
            render();
            break;
        }
        case 'ArrowDown': {
            presentation.down();
            render();
            break;
        }
     
        default: {
            console.log("default: " + e.key);
            break;
        }
    }
});

function initNav() {
    var array = [
        '<div class="cell"></div>',
        '<div class="cell"><button class="nav-button" id="up">↥</button></div>',
        '<div class="cell"></div>',
        '<div class="cell"><button class="nav-button" id="left">↤</button></div>',
        '<div class="cell"></div>',
        '<div class="cell"><button class="nav-button" id="right">↦</button></div>',
        '<div class="cell"></div>',
        '<div class="cell"><button class="nav-button" id="down">↧</button></div>',
        '<div class="cell"></div>'
    ];

    array.forEach(function (cell) {
        nav.innerHTML += cell;
    });

    document.getElementById('right')!.addEventListener('click', (e: Event) => {
        e.preventDefault();
        presentation.next();
        render();
    });

    document.getElementById('up')!.addEventListener('click', (e: Event) => {
        e.preventDefault();
        presentation.up();
        render();
    });

    document.getElementById('down')!.addEventListener('click', (e: Event) => {
        e.preventDefault();
        presentation.down();
        render();
    });

    document.getElementById('left')!.addEventListener('click', (e: Event) => {
        e.preventDefault();
        presentation.prev();
        render();
    });
}

function render() {
    const currentSlide = presentation.currentSlide();

    document.querySelector<HTMLDivElement>('#app')!.innerHTML = currentSlide.content;
    window.location.hash = presentation.currentSlide().id;

    const buttons = Array.from(document.getElementsByClassName("nav-button"));

    buttons.forEach(btn => {
        btn.setAttribute("style", "display: none");
    });

    if (presentation.hasUp()) {
        document.getElementById("up")!.style.display = 'block';
    }

    if (presentation.hasDown()) {
        document.getElementById("down")!.style.display = 'block';

    }

    if (presentation.hasPrev()) {
        document.getElementById("left")!.style.display = 'block';

    }

    if (presentation.hasNext()) {
        document.getElementById("right")!.style.display = 'block';
    }
}
